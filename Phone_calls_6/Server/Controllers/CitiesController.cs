﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phone_calls_6.Shared.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_calls_6.Server.Controllers
{
    [Route("City")]
    [ApiController]
    public class CitiesController : Controller
    {
        #region Private Fields

        private readonly PhoneCallsContext _context;

        #endregion Private Fields

        #region Public Constructors

        public CitiesController(PhoneCallsContext context)
        {
            _context = context;
        }

        #endregion Public Constructors

        #region Public Methods

        [HttpGet]
        public async Task<ActionResult<IEnumerable<City>>> GetCities()
        {
            return await _context.Cities.ToListAsync();
        }

        // GET: City/Details/5
        [HttpGet("{id}")]
        public async Task<ActionResult<City>> GetCities(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var City = await _context.Cities
                .FirstOrDefaultAsync(m => m.id == id);
            if (City == null)
            {
                return NotFound();
            }

            return City;
        }

        [HttpPut]
        public async Task<IActionResult> PutCity(City city)
        {
            _context.Entry(city).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(city.id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<City>> PostCity(City city)
        {
            _context.Cities.Add(city);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCities", new { city.id }, city);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<City>> DeleteCity(int id)
        {
            var City = await _context.Cities.FindAsync(id);
            _context.Cities.Remove(City);
            await _context.SaveChangesAsync();
            return City;
        }

        #endregion Public Methods

        #region Private Methods

        private bool CityExists(int id)
        {
            return _context.Cities.Any(e => e.id == id);
        }

        #endregion Private Methods
    }
}