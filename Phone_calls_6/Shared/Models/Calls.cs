using System;

namespace Phone_calls_6.Shared.Models
{
    public class Calls
    {
        #region Public Properties

        public int id { get; set; }
        public Persons user { get; set; }
        public DateTime date_of_calls { get; set; }
        public float duration { get; set; }
        public City city { get; set; }

        #endregion Public Properties
    }
}